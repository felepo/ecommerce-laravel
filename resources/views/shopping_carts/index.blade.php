@extends('layouts.app')

@section('content')
	<div class="big-padding text-center blue-grey white-text">
		<h1>Tu Carrito de Compras</h1>
	</div>

	<div class="container">
		<table class="table table-bordered">
			<thead>
				<tr>
					<td><strong>Producto</strong></td>
					<td><strong>Precio</strong></td>
				</tr>
			</thead>
			<tbody>
				@foreach($products as $product)
					<tr>
						<td>{{$product->title}}</td>
						<td>{{$product->pricing}}</td>
					</tr>
				@endforeach
				<tr>
					<td><strong>Total</strong></td>
					<td><strong>{{$total}}</strong></td>
				</tr>
			</tbody>
		</table>
	</div>
@endsection