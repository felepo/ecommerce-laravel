<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\ShoppingCart;

class ShoppingCartProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //Esto ayuda a inyectar variables dentro de las vistas.
        //El primer parámetro es a qué vista incluir las variables. El * significa todos.
        view()->composer('*', function($view)
        {
            //Instrucciones para poder usar el carrito de compras
            $shopping_cart_id = \Session::get('shopping_cart_id');
            $shopping_cart = ShoppingCart::findOrCreateBySessionID($shopping_cart_id);
            \Session::put('shopping_cart_id', $shopping_cart->id);

            //Enviar la variable $shopping_cart
            $view->with('productsCount', $shopping_cart->productsSize());
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
