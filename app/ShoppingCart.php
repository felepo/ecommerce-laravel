<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
    //Mass assignment --> es para que 'status' se pueda asignar en el metodo
    // create() de ShoppingCart en createWithoutSession()
    protected $fillable = ['status'];

    //Devuelve los registros de la tabla in_shopping_carts donde el shopping_cart_id sea el actual
    public function inShoppingCarts()
    {
        return $this->hasMany('App\InShoppingCart');
    }

    //Devuelve todos los productos que esten en ShoppingCart
    public function products()
    {
        return $this->belongsToMany('App\Product', 'in_shopping_carts');
    }

    //Metodo para saber cuantos productos existen en el carrito de compras
    public function productsSize()
    {
    	return $this->products()->count();
    }

    //Metodo para sacar el precio total de los productos
    public function total()
    {
        return $this->products()->sum('pricing');
    }

    public function totalUSD()
    {
        return $this->products()->sum('pricing') / 100;
    }    

    public static function findOrCreateBySessionID($shopping_cart_id)
    {
        if( $shopping_cart_id )
        {
            //Buscar el carrito de compras con este ID
            return ShoppingCart::findBySession($shopping_cart_id);
        }
        else
        {
            //Crear el carrito de compras
            return ShoppingCart::createWithoutSession();
        }
    }

    public static function findBySession($shopping_cart_id)
    {
        return ShoppingCart::find($shopping_cart_id);
    }

    public static function createWithoutSession()
    {
        return ShoppingCart::create([
            'status' => 'incompleted'
        ]);
    }
}
