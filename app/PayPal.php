<?php

namespace App;

class PayPal
{
	private $_apiContext;
	private $shopping_cart;
	private $_ClientId = 'AYd2gPMrXr_vx1itI8ZFD_QnyEt-BWQBluOQJ8dAd4kaIsdOuFPyxllUbDXb0pVrVhyw6YXajR7whvNm';
	private $_ClientSecret = 'EHQC5wt2C8jnfWNxDYB-5lETVIXWkNAeqrpe3DswiJWbzgDAzC5cH7cFuMlbbJW0_iYFRKsRgIyRQrK1';

	public function __construct($shopping_cart)
	{
		$this->_apiContext = \Paypalpayment::ApiContext($this->_ClientId, $this->_ClientSecret);
		
		$config = config('paypal_payment');
		$flatConfig = array_dot($config);

		$this->_apiContext->setConfig($flatConfig);
		$this->shopping_cart = $shopping_cart;
	}

	public function generate()
	{
		$payment = \Paypalpayment::payment()->setIntent('sale')
			->setPayer($this->payer())
			->setTransactions([$this->transaction()])		//Es necesario pasarle los valores en un arreglo
			->setRedirectUrls($this->redirectURLs());

		try
		{
			$payment->create($this->_apiContext);
		}
		catch(\Exception $ex)
		{
			dd($ex);
			exit(1);
		}

		return $payment;
	}

	public function payer()
	{
		//Informacion sobre quien hara el pago
		return \Paypalpayment::payer()->setPaymentMethod('paypal');
	}

	public function redirectURLs()
	{
		//URLs de redireccionamiento
		$baseURL = url('/');

		return \Paypalpayment::redirectUrls()
			->setReturnUrl($baseURL.'/payments/store')
			->setCancelUrl($baseURL.'/cart');
	}

	public function transaction()
	{
		//Informacion sobre la transaccion que se hara, o sea, lo que se comprara
		return \Paypalpayment::transaction()
			->setAmount($this->amount())
			->setItemList($this->items())
			->setDescription('Tu compra desde TIENDA FELEPO')
			->setInvoiceNumber(uniqid());
	}

	public function items()
	{
		$items = [];

		$products = $this->shopping_cart->products()->get();

		foreach ($products as $product) {
			array_push($items, $product->paypalItem());
		}

		return \Paypalpayment::itemList()->setItems($items);
	}

	public function amount()
	{
		return \Paypalpayment::amount()
			->setCurrency('USD')
			->setTotal($this->shopping_cart->totalUSD());
	}

	public function execute($paymentId, $payerId)
	{
		$payment = \Paypalpayment::getById($paymentId, $this->_apiContext);

		$execution = \Paypalpayment::PaymentExecution()->setPayerId($payerId);

		return $payment->execute($execution, $this->_apiContext);
	}
}