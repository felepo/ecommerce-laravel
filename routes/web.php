<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@home');

Route::get('/cart', 'ShoppingCartsController@index');

Route::get('/payments/store', 'PaymentsController@store');

Auth::routes();

Route::resource('products','ProductsController');
 
//Con el arreglo le digo que solo estarán habilitadas las opciones de store y destroy
Route::resource('in_shopping_carts', 'InShoppingCartsController', [
	'only' => ['store', 'destroy']
]);

Route::get('/home', 'HomeController@index')->name('home');
